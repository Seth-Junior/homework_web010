import React, { useState, useEffect } from "react";
import {Container,Table,Button,Col,Row,Form,Image,ButtonGroup} from "react-bootstrap";
import {fetchAllAuthors,putAuthor,postAuthor,deleteAuthorById,uploadImage} from "../services/author_services";


export default function Home() {
    const [authors, setAuthors] = useState([]);
    const [id, setId] = useState("");
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [imageUrl, setImageUrl] = useState();
    const [imageFile, setImageFile] = useState(null);
    const [isUpdating, setIsUpdating] = useState(false);
    const [needsReloading, setNeedsReloading] = useState(false);

    useEffect(() => {
        const fetch = async () => {
            const results = await fetchAllAuthors();
            setAuthors(results);
        };
        fetch();
        if (needsReloading) setNeedsReloading(false);
    }, [needsReloading]);

    const clearForm = (e) => {
        setId("");
        setName("");
        setEmail("");
        setImageUrl();
        setImageFile("");
    };

    const onAdd = async (e) => {
        e.preventDefault();

        if (email) {
            const tempAuthor = {
                name,
                email,
            };

            if (imageFile) {
                let url = await uploadImage(imageFile);
                tempAuthor.image = url;
                console.log(imageFile);
            }

            if (isUpdating) {
                putAuthor(id, tempAuthor).then((message) => alert(message));
                setIsUpdating(false);
                setNeedsReloading(true);
            } else {
                postAuthor(tempAuthor).then((message) => alert(message));
                setNeedsReloading(true);
            }

            clearForm(e);
            setIsUpdating(false);
        }
    };

    const onDelete = async (e, id) => {
        setNeedsReloading(true);
        deleteAuthorById(id).then((message) => alert(message));
        const temp = authors.filter((author) => {
            return author._id !== id;
        });
        setAuthors(temp);
    };

    const onEdit = async (e, id, name, email, imgUrl) => {
        e.preventDefault();
        if (isUpdating) {
            clearForm(e);
            setIsUpdating(false);
        } else {
            setNeedsReloading(true);
            setIsUpdating(true);
            setId(id);
            setName(name);
            setEmail(email);
            setImageUrl(imgUrl);
        }
    };

    return (
        <div>
            <Container>
                <Col>
                    <Row className={"mb-4"}>
                        <Col xs={8}>
                            <Form style={{ width: "90%" }}>
                                <Form.Group controlId='formBasicName'>
                                    <Form.Label>Name</Form.Label>
                                    <Form.Control
                                        type='text'
                                        placeholder='Enter name'
                                        onChange={(e) => setName(e.target.value)}
                                        value={name}
                                    />
                                </Form.Group>
                                <Form.Group controlId='formBasicEmail' className={"mt-3"}>
                                    <Form.Label>Email address</Form.Label>
                                    <Form.Control
                                        type='email'
                                        placeholder='Enter email'
                                        onChange={(e) => setEmail(e.target.value)}
                                        value={email}
                                    />
                                </Form.Group>
                                <Form.Text
                                    style={{ color: "red", display: "block" }}
                                    hidden={setEmail}>
                                    Please provide a valid Email . (Ex: sample12@gmail.com)
                                </Form.Text>
                                <ButtonGroup className={"my-3"} aria-label='Basic example'>
                                    <Button
                                        variant='primary'
                                        type='submit'
                                        onClick={(e) => onAdd(e)}>
                                        {isUpdating ? "Save" : "Submit"}
                                    </Button>
                                    <Button
                                        hidden={!isUpdating}
                                        variant='secondary'
                                        type='submit'
                                        onClick={(e) => {
                                            clearForm(e);
                                            setIsUpdating(false);
                                        }}>
                                        Cancel
                                </Button>
                                </ButtonGroup>
                            </Form>
                        </Col>
                        <Col xs={4} className={"mt-4"}>
                            <Image src={imageUrl} className={"w-100"} />
                            <Form>
                                <Form.File
                                    id='img'
                                    className={"py-2"}
                                    onChange={(e) => {
                                        let url = URL.createObjectURL(e.target.files[0]);
                                        setImageFile(e.target.files[0]);
                                        setImageUrl(url);
                                    }}
                                />
                            </Form>
                        </Col>
                    </Row>
                    <Table bordered hover>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Image</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {authors.map((author, index) => (
                                <tr key={index}>
                                    <td>{index + 1}</td>
                                    <td>{author.name}</td>
                                    <td>{author.email}</td>
                                    <td>
                                        <Image src={author.image} style={{width:'250px',height:'200px'}}/>
                                    </td>
                                    <td>
                                        <Button
                                            variant='warning'
                                            onClick={(e) =>
                                                onEdit(
                                                    e,
                                                    author._id,
                                                    author.name,
                                                    author.email,
                                                    author.image
                                                )
                                            }>
                                            {isUpdating && id === author._id ? "Cancel" : "Edit"}
                                        </Button>
                                        <Button
                                            disabled={isUpdating}
                                            variant='danger'
                                            onClick={(e) => onDelete(e, author._id)}>
                                            Delete
                                        </Button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </Col>
            </Container>
        </div>
    );
}
