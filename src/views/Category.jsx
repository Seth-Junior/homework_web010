import React from "react";
import { Container, Form, Table,Button } from "react-bootstrap";

const Category = () => {



  return (
    <Container>
      <h1 className="my-3">Category</h1>
      <Form>
        <Form.Group controlId="category">
          <Form.Label>Category Name</Form.Label>
          <Form.Control type="text" placeholder="Category Name" />
          <br/>
          <Button variant="secondary">Add</Button>
          <Form.Text className="text-muted"></Form.Text>
        </Form.Group>
      </Form>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>Name</td>
            <td>
              <Button
                size="sm"
                variant="warning"
              >
                Edit
              </Button>{" "}
              <Button
                size="sm"
                variant="danger"
              >
                Delete
              </Button>
            </td>
          </tr>
        </tbody>
      </Table>
    </Container>
  );
};

export default Category;
