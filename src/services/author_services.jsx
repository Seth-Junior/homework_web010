import api from "../api/api"

export const fetchAllAuthors = async () => {
  try {
    const results = await api.get("author");
    return results.data.data;
  } catch (err) {
    console.log(err);
  }
};

export const deleteAuthorById = async (id) => {
  try {
    const result = await api.delete(`author/${id}`);
    console.log("fetchAllArticles:", result.data.data);
    return result.data.message;
  } catch (error) {
    console.log("fetchAllArticles Error:", error);
  }
};

export const postAuthor = async (author) => {
  try {
    const result = await api.post("author", author);
    console.log("fetchAllArticles:", result.data.message);
    return result.data.message;
  } catch (error) {
    console.log("fetchAllArticles Error:", error);
  }
};

export const putAuthor = async (_id, author) => {
  try {
    const result = await api.put(`author/${_id}`, author);
    console.log("fetchAllArticles:", result.data.message);
    return result.data.message;
  } catch (error) {
    console.log("fetchAllArticles Error:", error);
  }
};

export const uploadImage = async (file) => {
  let formData = new FormData();
  formData.append("image", file);

  let response = await api.post("images", formData);
  return response.data.url;
};
